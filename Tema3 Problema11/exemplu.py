from Colonie import Grup

grupCelule = Grup()

nrGeneratii = int(input("Introduceti numarul de generatii noi: "))

nr = 0
while nrGeneratii != 0:
    nr += 1
    print("Generatia ", nr, ": ")
    grupCelule.evolutie()
    nrGeneratii -= 1

grupCelule.afisare()