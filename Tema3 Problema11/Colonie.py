class Grup:

    # Constructorul clasei
    def __init__(self):
        # Citim de la tastatura numarul de linii si numarul de coloane
        self.nrL = int(input("Introduceti numarul de linii: "))
        self.nrC = int(input("Introduceti numarul de coloane: "))

        self.matrix = [[0 for i in range(self.nrC)] for j in range(self.nrL)]

        # Citim elementele matricei
        print("Introduceti matricea de celule (0-celula este moarta / 1-celula este in viata): ")
        for i in range(self.nrL):
            self.matrix[i] = list(map(int, input().split()))

    # Functia returneaza numarul de celule vecine in viata ale celulei din pozitia (i, j)
    def veciniVii(self, i, j):
        dx = [-1, 0, 1]
        dy = [-1, 0, 1]

        nr = 0
        for x in range(3):
            for y in range(3):
                if dx[x] == 0 and dy[y] == 0:
                    continue

                newI = i + dx[x]
                newJ = j + dy[y]
                if newI >= 0 and newI < self.nrL and newJ >= 0 and newJ < self.nrC and self.matrix[newI][newJ] == 1:
                    nr += 1
        return nr

    def evolutie(self):
        celuleCareInvie = [] # Lista retine pozitiile celulelor moarte care invie in generatia urmatoare
        celuleCareMor = [] # Lista retine pozitiile celulelor vii care mor in generatia urmatoare

        for i in range(self.nrL):
            for j in range(self.nrC):
                nr = self.veciniVii(i, j)

                if self.matrix[i][j] == 1 and (nr < 2 or nr > 3):
                    # Celula moare
                    # Marcam elementul din pozitia (i, j) ca fiind modificat la acest nivel de evolutie introducandu-i pozitia in lista corespunzatoare
                    celuleCareMor.append((i, j))

                elif self.matrix[i][j] == 0 and nr == 3:
                    # Celula devine vie
                    # Marcam elementul din pozitia (i, j) ca fiind modificat la acest nivel de evolutie introducandu-i pozitia in lista corespunzatoare
                    celuleCareInvie.append((i, j))

        # Actualizam matricea, celulele moarte din lista devin vii
        for k in celuleCareInvie:
            p = k[0]
            q = k[1]
            self.matrix[p][q] = 1 - self.matrix[p][q]

        # Actualizam matricea, celulele vii din lista mor
        for k in celuleCareMor:
            p = k[0]
            q = k[1]
            self.matrix[p][q] = 1 - self.matrix[p][q]

        # Afisam celulele modificate
        print("Celulele care invie in aceasta generatie sunt: ")
        for k in celuleCareInvie:
            print('({0}, {1})'.format(k[0], k[1]), end = ' ')
        print('\n')

        print("Celulele care mor in aceasta generatie sunt: ")
        for k in celuleCareMor:
            print('({0}, {1})'.format(k[0], k[1]), end=' ')
        print('\n')

        celuleCareInvie.clear()
        celuleCareMor.clear()

    # Functia afiseaza matricea
    def afisare(self):
        for i in range(self.nrL):
            for j in range(self.nrC):
                    print(self.matrix[i][j], end = ' ')
            print('\n')

