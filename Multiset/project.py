# Imports all the functionalities of the multiset library
from multiset import *

# Initializing a Multiset object named multiset
multiset = Multiset([0, 1, 1, 1, 2, 2, 3])

# __repr__ method demo
print(multiset)

# __len__ method demo
print(len(multiset))

# isEmpty method demo: checks if multiset is empty (and it isn't)
if multiset and not multiset.isEmpty():
    print('Not empty!')
else:
    print('Empty!')

# getSupport method demo: prints the unique elements from the multiset
print(multiset.getSupport())

# __contains__ method demo: checks if element 1 is in multiset
if 1 in multiset:
    print('Element found!')
else:
    print('Element not found!')

# k method demo: same as count method, returns count of element 1 in multiset
print(multiset.k(1))

# Initializing two Multiset objects m1 and m2 for basic operations with multiset
m1 = Multiset([0, 1, 1, 1, 3, 4])
m2 = Multiset([0, 1, 1, 3, 4])

# isIncludedIn method: prints whether m1 is included in m2 or not
print(m1.isIncludedIn(m2))

# intersection method: prints the intersection of m1 and m2
print(m1.intersection(m2))

# union method: prints the union of m1 and m2
print(m1.union(m2))

# difference method: prints the difference between m1 and m2
print(m1.difference(m2))

# difference method: demonstrates what happens if m2 is not included in m1
print(m2.difference(m1))

# basic comparison methods based on isIncludedIn method
print(m1 < m2)
print(m1 <= m2)
print(m1 == m2)
print(m1 != m2)
print(m1 > m2)
print(m1 >= m2)
