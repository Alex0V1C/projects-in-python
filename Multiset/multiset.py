class Multiset:
    # Constructor method: receive an object and tries
    # to convert it to a list, since our multiset elements are
    # stored as a list.
    def __init__(self, initList):
        self.elements = list(initList)

    # Representation method: simple and suggestive string representation
    def __repr__(self):
        return 'Multiset(' + repr(self.elements) + ')'

    # Length method: will return the length of the list of elements
    def __len__(self):
        return len(self.elements)

    # isEmpty method: return a boolean value
    # True if the list of elements is empty
    # False otherwise
    def isEmpty(self):
        return not bool(self.elements)

    # getSupport method: return a list of unique elements from the multiset
    # Uses set constructor to get the unique elements
    # then converts back to a list (just to keep data type uniformity)
    def getSupport(self):
        return list(set(self.elements))

    # Contains method: receive a parameter item and return a boolean value
    # True if the item is existant in the list
    # False otherwise
    def __contains__(self, item):
        return bool(item in self.elements)

    # Count method: receive a parameter value and return an integer
    # representing the number of elements
    # Will be further used by the "k" method defined in the homework statement
    # (just to keep the same notations as in the statement)
    def count(self, value):
        return self.elements.count(value)

    # k method: same functionality as count
    k = count

    # isIncludedIn method: receive a multiset object and return a boolean value:
    # True if self object is included in the other multiset
    # False otherwise
    # Functionality: for every value in our multiset we check two things:
    # 1) if this value exists in the other multiset
    # 2) if this value doesn't appear more times
    #    in our multiset than it appears in the other multiset
    def isIncludedIn(self, multiset):
        for value in self.getSupport():
            if not value in multiset.getSupport(): # 1)
                return False
            if self.k(value) > multiset.k(value): # 2)
                return False
        return True

    # intersection method: receive a multiset object and return a sorted list
    # consisting of the intersection between our multiset and the one received
    # Functionality: for every value in our multiset we check if the value
    #                is in the other multiset or not. If it is, then we insert
    #                it in our intersection list that will be finally returned
    def intersection(self, multiset):
        intersectionList = []
        for value in self.elements:
            if value in multiset.elements and intersectionList.count(value) < min(self.k(value), multiset.k(value)):
                intersectionList.append(value)
        return sorted(intersectionList)

    # union method: receive a multiset object and return a sorted list
    # consisting of the union between our multiset and the one received
    # Functionality: we use the following relationship
    # (A union B) = A + B - (A intersection B)
    # So, we build a new unionList = our multiset + other multiset
    # Then, we remove every element from the intersection and return the list.
    def union(self, multiset):
        unionList = self.elements.copy() + multiset.elements.copy()
        for value in self.intersection(multiset):
            unionList.remove(value)
        return sorted(unionList)

    # difference method: receive a multiset object and return a sorted list
    # consisting of the difference between our multiset and the one received
    # Functionality:
    # 1) We have to check if our multiset is included in the other multiset
    # 2) We initialize a differenceList = our multiset
    # 3) For every element in the other multiset, remove it from differenceList
    # 4) Return the differenceList at the end
    def difference(self, multiset):
        if not multiset.isIncludedIn(self):
            return 'Second multiset is not included in first multiset'

        differenceList = self.elements.copy()
        for value in multiset.elements:
            differenceList.remove(value)
            
        return sorted(differenceList)

    # Equality comparison: returns True if two multisets are equal
    def __eq__(self, other):
        return self.elements == other.elements

    # Non-equality comparison: returns True if two multisets are not equal
    def __ne__(self, other):
        return not self == other

    # Less than comparison: returns True if our multiset
    # is included in the other one and if the two multisets are not equal
    def __lt__(self, other):
        return self.isIncludedIn(other) and self != other

    # Less than or equal to comparison: returns True if our multiset
    # is included in the other one
    def __le__(self, other):
        return self.isIncludedIn(other)

    # Greater than comparison: returns True if the other multiset
    # is included in our multiset and if the two multisets are not equal
    def __gt__(self, other):
        return other.isIncludedIn(self) and self != other

    # Greater than or equal to comparison: returns True if the other multiset
    # is included in our multiset
    def __ge__(self, other):
        return other.isIncludedIn(self)
