from Carti_de_Telefon import *

#Introducem 3 abonati:
AdaugareAbonat() #Maria 0251336136
AdaugareAbonat() #Alin  0763193990
AdaugareAbonat() #Laura 0724418924

SalvareAgenda('Agenda') #Salvam datele in fisierul 'Agenda'
IncarcareAgenda('Agenda') #Preluam datele din fisierul 'Agenda'

StergereAbonat("Maria") #Testam functia de stergere
AfisareAgenda()  #Tstam funstia de afisare

SalvareAgenda('Agenda') #Salvam datele in fisierul 'Agenda'

StergereAgenda() #Stergem intreaga agenda, deci agenda este goala

CautareAbonat("Alin") #Deoarece agenda este goala funstia va afisa "Abonat inexistent"

IncarcareAgenda('Agenda') #Preluam datele din fisierul 'Agenda'
                          #Dictionarul agenda o sa aiba doi abonati: Alin si Laura

CautareAbonat("Alin") #Testam functia de cautare

StergereAgenda() #Stergem intreaga agenda