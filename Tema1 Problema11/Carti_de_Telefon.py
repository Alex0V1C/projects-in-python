#Pentru a realiza acest proiect vom utiliza dictionare
#"agenda" este dictionarul care va retine Numele si Numarul de telefon al fiecarui abonat
agenda = dict()


#In functia AdaugareAbonat citim datele (Nume si Nr. telefon) si cream un nou abonat
def AdaugareAbonat():
    name = input("Introduce numele noului abonat: ")

    #isValidNumber verifica daca numarul introdus de la tastatura e valid (daca are 10 cifre si prima cifra este 0)
    def isValidNumber(number):
        return len(number) == 10 and number[0] == '0' and number.isdigit()

    number = input("Introduceti numarul lui {0}: ".format(name))

    #Citim numere de la tastatura pana cand introducem un numar valid
    while not isValidNumber(number):
        number = input("Format numar incorect. Incercati din nou: ")

    #Actualizam agenda adaugand un nou abonat
    agenda.update({name: number})


#In functia urmatoare vom sterge un abonat dupa nume
def StergereAbonat(name):
    #Daca name exista in agenda stergem abonatul respectiv, in caz contrar afisam "Abonat inexistent"
    if name in agenda:
        del agenda[name]
    else:
        print("Abonat inexistent")


#Cautam un abonat dupa un nume dat
#Daca name exista in agenda stergem abonatul respectiv, in caz contrar afisam "Abonat inexistent"
def CautareAbonat(name):
    if name in agenda:
        print("Numarul abonatului cautat este: ", agenda[name])
    else:
        print("Abonat inexistent")


#Aceasta functie sterge intreaga agenda
def StergereAgenda():
    agenda.clear()


#Afisam agenda folosind functia format()
def AfisareAgenda():
    # List Comprehension
    print(*['Nr. {0}: {1} {2}'.format(index, nume, agenda[nume]) for index, nume in enumerate(agenda)], sep = '\n')



#Importam din biblioteca  pickle functiile dump si load
from pickle import dump, load

def SalvareAgenda(numeFisier):
    fisier = open(numeFisier, 'wb')
    dump(agenda, fisier) #dump rescrie agenda sub forma de biti in fisier

def IncarcareAgenda(numeFisier):
    fisier = open(numeFisier, 'rb')
    agenda.update(load(fisier)) #Incarca bitii din fisier si reconstruieste obiectul de tip dictionar


















