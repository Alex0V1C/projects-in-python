from Carti_de_Telefon import *

def menu():
    print("1. Adaugare abonat nou")
    print("2. Stergere abonat")
    print("3. Cautare abonat")
    print("4. Salvare date in fiser")
    print("5. Import date din fisier")
    print("6. Afisare agenda")
    print("7. Stergere agenda")
    print("8. Exit")


while (1):

    menu()

    ch = input()
    optiune = int(ch)

    if optiune == 1:
        AdaugareAbonat()
    elif optiune == 2:
        AfisareAgenda()
        name = input("Introduce-ti numele abonatului pe care doriti sa-l stergeti: ")
        StergereAbonat(name)
    elif optiune == 3:
        name = input("Introduceti numele abonatului pe care doriti sa-l cautati: ")
        CautareAbonat(name)
    elif optiune == 4:
        SalvareAgenda('Agenda')
    elif optiune == 5:
        IncarcareAgenda('Agenda')
    elif optiune == 6:
        AfisareAgenda()
    elif optiune == 7:
        StergereAgenda()
    else:
        break
