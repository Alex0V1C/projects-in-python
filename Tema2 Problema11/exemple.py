from Agenda import *

mylist = phoneBook()

mylist.addClient() #Maria 0251336136
mylist.addClient() #Alin  0763193990
mylist.addClient() #Laura 0724418924

mylist.displayPhoneBook()

mylist.saveData('Carte') # Salvam datele in fisierul 'Carte'
mylist.retrieveData('Carte') # Preluam datele din fisierul 'Carte'

mylist.deleteClient('Maria') # Testam functia de stergere
mylist.displayPhoneBook()  # Testam functia de afisare

mylist.saveData('Carte') # Salvam datele in fisierul ''Carte'

mylist.deletePhoneBook() # Stergem intreaga agenda, deci agenda este goala

mylist.searchClient('Alin') # Deoarece agenda este goala functia va afisa "Abonat inexistent"

mylist.retrieveData('Carte')# Preluam datele din fisierul 'Carte'
                            # Agenda o sa aiba doi abonati: Alin si Laura

mylist.searchClient('Alin') # Testam functia de cautare

mylist.deletePhoneBook() # Stergem intreaga agenda