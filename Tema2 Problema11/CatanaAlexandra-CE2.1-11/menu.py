from Agenda import *

def menu():
    print("1. Adaugare abonat nou")
    print("2. Stergere abonat")
    print("3. Cautare abonat")
    print("4. Salvare date in fiser")
    print("5. Import date din fisier")
    print("6. Afisare agenda")
    print("7. Stergere agenda")
    print("8. Exit")

mylist = phoneBook()

while (1):

    menu()

    ch = input()
    optiune = int(ch)

    if optiune == 1:
        mylist.addClient()
    elif optiune == 2:
        mylist.displayPhoneBook()
        name = input("Introduce-ti numele abonatului pe care doriti sa-l stergeti: ")
        mylist.deleteClient(name)
    elif optiune == 3:
        name = input("Introduceti numele abonatului pe care doriti sa-l cautati: ")
        mylist.searchClient(name)
    elif optiune == 4:
        mylist.saveData('Carte')
    elif optiune == 5:
        mylist.retrieveData('Carte')
    elif optiune == 6:
        mylist.displayPhoneBook()
    elif optiune == 7:
        mylist.deletePhoneBook()
    else:
        break
