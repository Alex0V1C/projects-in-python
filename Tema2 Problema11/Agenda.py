from pickle import dump, load

#clasa Client retine datele: nume si numar de telefon
class Client:
    def __init__(self, name, number):
        self.name = name
        self.number = number

    def __repr__(self):
        return self.name + ': ' + self.number


class phoneBook:

    def __init__(self):
        self.myPhoneBook = [] #lista myPhoneBook o sa retina elemente de tip Client


    def addClient(self):
        name = input("Introduceti numele noului abonat: ")

        # isValidNumber verifica daca numarul introdus de la tastatura e valid (daca are 10 cifre si prima cifra este 0)
        def isValidNumber(number):
            return len(number) == 10 and number[0] == '0' and number.isdigit()

        number = input("Introduceti numarul lui {0}: ".format(name))

        # Citim numere de la tastatura pana cand introducem un numar valid
        while not isValidNumber(number):
            number = input("Format numar incorect! Incercati din nou: ")

        s = Client(name, number) # Am creat un nou client in s
        self.myPhoneBook.append(s) # Adaugam clientul s in agenda

    # Functia sterge un client dupa nume
    def deleteClient(self, Name):
        firstLength = len(self.myPhoneBook)

        for index in range(len(self.myPhoneBook)):
            if self.myPhoneBook[index].name == Name:
                self.myPhoneBook.pop(index)
                break

        if firstLength == len(self.myPhoneBook):
            print("Abonatul nu a fost gasit!")

    # Functia cauta un abonat dupa nume si ii afiseaza datele daca il gaseste
    def searchClient(self, Name):
        isFound = 0

        for index in range(len(self.myPhoneBook)):
            if self.myPhoneBook[index].name == Name:
                print("Abonat gasit!")
                print(self.myPhoneBook[index])
                isFound = 1

        if isFound == 0:
            print("Abonatul nu a fost gasit!")

    # Functia sterge toti abonatii din agenda, agenda devine goala
    def deletePhoneBook(self):
        self.myPhoneBook.clear()

    # Functia afiseaza datele abonatilor din agenda
    def displayPhoneBook(self):
        for index, value in enumerate(self.myPhoneBook):
            print('Nr.' + str(index) + ' -> ' + repr(value))

    # Datele din agenda sunt salvate intr-un fisier dat sub forma de biti
    def saveData(self, numeFisier):
        fisier = open(numeFisier, 'wb')
        dump(self.myPhoneBook, fisier)  # dump rescrie agenda sub forma de biti in fisier

    # Agenda preia datele existente dintr-un fisier dat
    def retrieveData(self, numeFisier):
        fisier = open(numeFisier, 'rb')
        self.myPhoneBook = list(load(fisier))  # Incarca bitii din fisier si reconstruieste obiectul de tip dictionar